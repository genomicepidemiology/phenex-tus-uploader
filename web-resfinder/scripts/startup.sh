#!/usr/bin/env ash

## NGINX
rm -f /etc/nginx/conf.d/default.conf
cp /app/config/services/nginx.conf /etc/nginx/nginx.conf
cp /app/config/services/nginx.app.conf /etc/nginx/conf.d/app.conf
ln -s /app/scripts/aliases.sh /etc/profile.d/aliases.sh

if [ "$DEPLOYMENT" = "development" ]; then
  ## NodeJS
  cp /app/config/services/nginx.dev.conf /etc/nginx/conf.d/dev.conf
  yarn install
  npm run serve &
fi

if [ "$DEPLOYMENT" = "testing" ]; then
  ## NodeJS
  yarn install
  yarn run build
  cp -r /app/dist/* /usr/share/nginx/html
fi

## START WEB_SERVER IN FOREGROUND
nginx -g "daemon off;"
