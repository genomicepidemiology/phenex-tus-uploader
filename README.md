# Demonstration of tusfull client-server

Demo project for testing TUS client and TUS server using resfinder Vue.js app as fronend.

# Prerequisites

- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

# Installing

```bash
$ git clone git@bitbucket.org:genomicepidemiology/phenex-tus-uploader.git --recursive
$ cd phenex-tus-client-server
$ docker-compose build
$ docker-compose up
```

Development specific configuration is in docker-compose.override.yml which automaticaly override docker-compose.yml configuration unless specified otherwise.

# Access

To access application open your browser at `localhost:8090`

# Deployment

In production enviroment deploy with

```bash
$ docker-compose -f docker-compose.yml -f docker-compose.prod-override.yml up -d
```
instead of just docker-compose up which use docker-compose.override.yml development setting

# Authors
Martin Koliba
Lukasz Dynowski